# Import Modules
import numpy as np
import pandas as pd
import distance
import gc
import matplotlib.pyplot as plt
import seaborn as sns
# get_ipython().magic('matplotlib inline')


# Load Data
data_1 = pd.read_csv('FWE_TE_GS1_060917020946.csv')
data_2 = pd.read_csv('FWE_TE_GS2_060917033604.csv')
data_3 = pd.read_csv('FWE_TE_GS3_060917073953.csv')
data_4 = pd.read_csv('FWE_TE_GS4_060917074337.csv')
data_5 = pd.read_csv('FWE_TE_GS5_060917081237.csv')
data_6 = pd.read_csv('FWE_TE_GS6_061217065200.csv')
data_7 = pd.read_csv('FWE_TE_GS7_061217065716.csv')
data_8 = pd.read_csv('FWE_TE_GS8_061217093543.csv')
data_9 = pd.read_csv('FWE_TE_GS9_061217093811.csv')


# merge data into one dataframe
data = pd.concat([data_1, data_2, data_3, data_4, data_5, data_6, data_7, data_8, data_9], axis = 0, join = 'outer')
data = data.reset_index(drop = True)


# drop unnecessary information
data = data.drop(['price', 'ptype', 'Classification_Product_Type', 'ProductGroup',
                  'ReferenceIndustry', 'ReferenceClass', 'PN', 'Remainder'], axis = 1)


# remove unnecessary spaces
for i in data.columns:
    if data[i].dtype == object:
        data[i] = data[i].str.strip()


# store and remove item information; leave only the information matrix
holder = data[['title', 'description', 'category']]
holder = pd.DataFrame(holder)
print('Holder Shape: ', holder.shape)

data = data.drop(['title', 'description', 'category'], axis=1)


# transform sparse information matrix into a binary sparse matrix
df = data
df = df.notnull().astype('int')

print('df Shape: ', df.shape)


# remove last column -- it doesn't belong
columns = df.columns
columns = columns[0:(len(columns)-1)] #if this doesn't work, use [0:288]

df = df[columns]
print('df Shape: ', df.shape)


# remove all rows without *any* 1s -- they do not have any information to train on
keep = df.loc[((df != 0).any(1))]

print('Keep Shape: ', keep.shape)
print('Holder Shape: ', holder.shape)


# remove empty rows
data = keep.join(holder, how='inner')
data = data.reset_index(drop=True)

print('Data Shape: ', data.shape)

print('Unique Data Categories: ')
print(data.category.unique())

print('Data Columns: ')
print(data.columns.ravel())

# reset index
data = data.reset_index(drop=True)


# again seperate feature information from item data (i.e. titles, category, etc.)
cols = data.columns[0:(len(data.columns)-3)]
df = data[cols]
print(df.shape)


# move name columns to left hand side:
name = data[['title', 'category']]
name.columns = ['title', 'category']
data = pd.concat([name, df], axis=1)


# must sort by category, then reset index (or else errors values will happen in the
# Hamming Distance code b/c the indices won't all be next to each other
data = data.sort_values(by='category')
data = data.reset_index(drop=True)


# create a list of all unique categories in dataset
categories = []

for i in data.category.unique():
    categories.append(i)

# create dataframe with no rows -- will use this to input final prediction values
whole_data = data.drop(['category'], axis=1)[:0]

for i in categories:
    print(i)
    
    sample = data[data['category'] == i]
    print(sample.shape)
    
    # this is an arbitrary cut off -- can change it to whatever you want
    if sample.shape[0] > 20:
        sample = sample.T
        sample = sample.drop(['category', 'title'], axis=0)

        # function that transforms sample data into dictionary
        def prepare_data(sample):
            
            sample = sample.astype(str)            
            dictionary = {}
            
            for j in sample.columns:
                holder = ''

                for k in sample[j]:
                    holder = holder + k
                    
                dictionary[j] = holder
            
            print(j)
            print(holder)
            print()
            
            return dictionary

        # function that finds the hamming distance of all itmes in a category, and converts it to a matrix
        def hamming_distance(dictionary):

            empty_data = pd.DataFrame(index = sample.columns, columns = sample.columns)

            x = sample.columns[0]
            for a in range(sample.columns[0], sample.columns[0] + len(dictionary)):
                for b in range(x, sample.columns[0] + len(dictionary)):
                    empty_data.loc[a][b] = distance.hamming(dictionary.get(a), dictionary.get(b))
                    empty_data.loc[b][a] = empty_data.loc[a][b]
                x = x + 1
            
            # return matrix of Hamming Scores
            return empty_data

        dictionary = prepare_data(sample)
        hamming_scores = hamming_distance(dictionary)
        

        # Center and normalize Hamming Scores
        maxim = np.max(hamming_scores.max())
        minim = np.min(hamming_scores.min())

        difference = maxim - minim
        half = (difference / 2) + minim
        
        # need this because, on one or two rare occasions, the distance is 0 which creates a 
        # "divide by zero error"
        if distance == 0:
            hamming_scores_aug = 0
        
        else:
            hamming_scores_aug = (half - hamming_scores) / difference


        results = np.dot(hamming_scores_aug, sample.T)
        results = pd.DataFrame(results)
        results = results.set_index(keys=hamming_scores_aug.index)
        results.columns = sample.T.columns
        
        whole_data = pd.concat([whole_data, results], axis=0)
    
    # when there are 20 items or less in a category, just return the original binary sparse matrix
    # in this situations, there isn't enough data to properly train the items.
    else:
        sample = sample.drop(['category'], axis=1)
        whole_data = pd.concat([whole_data, sample], axis=0)
    
    print()

print(whole_data.shape)
print(whole_data)

whole_data.to_csv('Predicted_Data.csv')